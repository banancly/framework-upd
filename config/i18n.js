module.exports = {
  enabled: true,
  preload: ['en', 'ru'],
  fallbackLng: 'en',
  saveMissing: false,
  debug: false,
  lookupQuerystring: 'lng', // string to detect language on query
};
